GitHub Search
=

![GitHub Search](https://bitbucket.org/krasavella/repositoryviewer/raw/d3f075affe44c9c1b9f5c3fbbdd18b2f125a78f4/samplie_images/screen_search.png)


The ability to search the repository by name.

Detailed information about the repository.