package com.evgeny.presentation.model

import com.evgeny.domain.model.Limit
import com.evgeny.domain.model.Resources
import com.evgeny.domain.model.Search
import javax.inject.Inject

data class LimitItem(val resources: ResourcesItem)

class LimitItemMapper @Inject constructor(
        private val resourcesItemMapper: ResourcesItemMapper
) {
    fun mapToPresentation(entity: Limit): LimitItem = LimitItem(
            resources = resourcesItemMapper.mapToPresentation(entity.resources)
    )

    fun mapToPresentation(list: List<Limit>): List<LimitItem> = list.map { mapToPresentation(it) }

    fun mapToDomain(entity: LimitItem): Limit = Limit(
            resources = resourcesItemMapper.mapToDomain(entity.resources)
    )

    fun mapToDomain(list: List<LimitItem>): List<Limit> = list.map { mapToDomain(it) }
}


data class ResourcesItem(val search: SearchItem)
class ResourcesItemMapper @Inject constructor(
        private val searchItemMapper: SearchItemMapper
) {
    fun mapToPresentation(entity: Resources): ResourcesItem = ResourcesItem(
            search = searchItemMapper.mapToPresentation(entity.search)
    )

    fun mapToPresentation(list: List<Resources>): List<ResourcesItem> = list.map { mapToPresentation(it) }

    fun mapToDomain(entity: ResourcesItem): Resources = Resources(
            search = searchItemMapper.mapToDomain(entity.search)
    )

    fun mapToDomain(list: List<ResourcesItem>): List<Resources> = list.map { mapToDomain(it) }
}


data class SearchItem(val limit: Int = 0,
                      val remaining: Int = 0,
                      val reset: Int = 0)

class SearchItemMapper @Inject constructor(
) {
    fun mapToPresentation(entity: Search): SearchItem = SearchItem(
            limit = entity.limit,
            remaining = entity.remaining,
            reset = entity.reset
    )

    fun mapToPresentation(list: List<Search>): List<SearchItem> = list.map { mapToPresentation(it) }

    fun mapToDomain(entity: SearchItem): Search = Search(
            limit = entity.limit,
            remaining = entity.remaining,
            reset = entity.reset
    )

    fun mapToDomain(list: List<SearchItem>): List<Search> = list.map { mapToDomain(it) }
}