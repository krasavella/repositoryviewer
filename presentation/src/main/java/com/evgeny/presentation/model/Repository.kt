package com.evgeny.presentation.model

import com.evgeny.domain.model.RepositoriesItem
import com.evgeny.domain.model.Owner
import com.evgeny.domain.model.Repository
import java.io.Serializable
import javax.inject.Inject

data class RepositoryItem(
        val totalCount: Int = 0,
        val incompleteResults: Boolean = false,
        val items: List<com.evgeny.presentation.model.RepositoriesItem>
) : Serializable

/**
 * Presentation layer should be responsible of mapping the domain model to an
 * appropriate presentation model and the presentation model to a domain model if needed.
 *
 * This is because domain should contain only business logic
 * and shouldn't know at all about presentation or data layers.
 */
class RepositoryItemMapper @Inject constructor(
        private val repositoriesItemMapper: RepositoriesItemMapper
) {

    fun mapToPresentation(entity: Repository): RepositoryItem = RepositoryItem(
            totalCount = entity.totalCount,
            incompleteResults = entity.incompleteResults,
            items = repositoriesItemMapper.mapToPresentation(entity.items)
    )

    fun mapToPresentation(list: List<Repository>): List<RepositoryItem> = list.map { mapToPresentation(it) }

    fun mapToDomain(entity: RepositoryItem): Repository = Repository(
            totalCount = entity.totalCount,
            incompleteResults = entity.incompleteResults,
            items = repositoriesItemMapper.mapToDomain(entity.items)
    )

    fun mapToDomain(list: List<RepositoryItem>): List<Repository> = list.map { mapToDomain(it) }
}

const val REPOSITORIES_ID_KEY = "USER_ID_KEY"

data class RepositoriesItem(
        val id: Int? = 0,
        val nodeId: String? = "",
        val name: String? = "",
        val owner: OwnerItem,
        val fullName: String?,
        val _private: Boolean? = false,
        val htmlUrl: String? = "",
        val description: String? = "",
        val fork: Boolean? = false,
        val url: String? = "",
        val forksUrl: String? = "",
        val keysUrl: String? = "",
        val collaboratorsUrl: String? = "",
        val teamsUrl: String? = "",
        val hooksUrl: String? = "",
        val issueEventsUrl: String? = "",
        val eventsUrl: String? = "",
        val assigneesUrl: String? = "",
        val branchesUrl: String? = "",
        val tagsUrl: String? = "",
        val blobsUrl: String? = "",
        val gitTagsUrl: String? = "",
        val gitRefsUrl: String? = "",
        val treesUrl: String? = "",
        val statusesUrl: String? = "",
        val languagesUrl: String? = "",
        val stargazersUrl: String? = "",
        val contributorsUrl: String? = "",
        val subscribersUrl: String? = "",
        val subscriptionUrl: String? = "",
        val commitsUrl: String? = "",
        val gitCommitsUrl: String? = "",
        val commentsUrl: String? = "",
        val issueCommentUrl: String? = "",
        val contentsUrl: String? = "",
        val compareUrl: String? = "",
        val mergesUrl: String? = "",
        val archiveUrl: String? = "",
        val downloadsUrl: String? = "",
        val issuesUrl: String? = "",
        val pullsUrl: String? = "",
        val milestonesUrl: String? = "",
        val notificationsUrl: String? = "",
        val labelsUrl: String? = "",
        val releasesUrl: String? = "",
        val deploymentsUrl: String? = "",
        val createdAt: String? = "",
        val updatedAt: String? = "",
        val pushedAt: String? = "",
        val gitUrl: String? = "",
        val sshUrl: String? = "",
        val cloneUrl: String? = "",
        val svnUrl: String? = "",
        val size: Int? = 0,
        val stargazersCount: Int? = 0,
        val watchersCount: Int? = 0,
        val language: String? = "",
        val hasIssues: Boolean? = false,
        val hasProjects: Boolean? = false,
        val hasDownloads: Boolean? = false,
        val hasWiki: Boolean? = false,
        val hasPages: Boolean? = false,
        val forksCount: Int? = 0,
        val archived: Boolean? = false,
        val openIssuesCount: Int? = 0,
        val forks: Int? = 0,
        val openIssues: Int? = 0,
        val watchers: Int? = 0,
        val defaultBranch: String? = "",
        val score: Double? = 0.0
) : Serializable

class RepositoriesItemMapper @Inject constructor(
        private val ownerItemMapper: OwnerItemMapper
) {

    fun mapToPresentation(repositoriesItem: RepositoriesItem): com.evgeny.presentation.model.RepositoriesItem = com.evgeny.presentation.model.RepositoriesItem(
            id = repositoriesItem.id,
            nodeId = repositoriesItem.nodeId,
            name = repositoriesItem.name,
            owner = ownerItemMapper.mapToPresentation(repositoriesItem.owner),
            fullName = repositoriesItem.fullName,
            _private = repositoriesItem._private,
            htmlUrl = repositoriesItem.htmlUrl,
            description = repositoriesItem.description,
            fork = repositoriesItem.fork,
            url = repositoriesItem.url,
            forksUrl = repositoriesItem.forksUrl,
            keysUrl = repositoriesItem.keysUrl,
            collaboratorsUrl = repositoriesItem.collaboratorsUrl,
            teamsUrl = repositoriesItem.teamsUrl,
            hooksUrl = repositoriesItem.hooksUrl,
            issueEventsUrl = repositoriesItem.issueEventsUrl,
            eventsUrl = repositoriesItem.eventsUrl,
            assigneesUrl = repositoriesItem.assigneesUrl,
            branchesUrl = repositoriesItem.branchesUrl,
            tagsUrl = repositoriesItem.tagsUrl,
            blobsUrl = repositoriesItem.blobsUrl,
            gitTagsUrl = repositoriesItem.gitTagsUrl,
            gitRefsUrl = repositoriesItem.gitRefsUrl,
            treesUrl = repositoriesItem.treesUrl,
            statusesUrl = repositoriesItem.statusesUrl,
            languagesUrl = repositoriesItem.languagesUrl,
            stargazersUrl = repositoriesItem.stargazersUrl,
            contributorsUrl = repositoriesItem.contributorsUrl,
            subscribersUrl = repositoriesItem.subscribersUrl,
            subscriptionUrl = repositoriesItem.subscriptionUrl,
            commitsUrl = repositoriesItem.commitsUrl,
            gitCommitsUrl = repositoriesItem.gitCommitsUrl,
            commentsUrl = repositoriesItem.commentsUrl,
            issueCommentUrl = repositoriesItem.issueCommentUrl,
            contentsUrl = repositoriesItem.contentsUrl,
            compareUrl = repositoriesItem.compareUrl,
            mergesUrl = repositoriesItem.mergesUrl,
            archiveUrl = repositoriesItem.archiveUrl,
            downloadsUrl = repositoriesItem.downloadsUrl,
            issuesUrl = repositoriesItem.issuesUrl,
            pullsUrl = repositoriesItem.pullsUrl,
            milestonesUrl = repositoriesItem.milestonesUrl,
            notificationsUrl = repositoriesItem.notificationsUrl,
            labelsUrl = repositoriesItem.labelsUrl,
            releasesUrl = repositoriesItem.releasesUrl,
            deploymentsUrl = repositoriesItem.deploymentsUrl,
            createdAt = repositoriesItem.createdAt,
            updatedAt = repositoriesItem.updatedAt,
            pushedAt = repositoriesItem.pushedAt,
            gitUrl = repositoriesItem.gitUrl,
            sshUrl = repositoriesItem.sshUrl,
            cloneUrl = repositoriesItem.cloneUrl,
            svnUrl = repositoriesItem.svnUrl,
            size = repositoriesItem.size,
            stargazersCount = repositoriesItem.stargazersCount,
            watchersCount = repositoriesItem.watchersCount,
            language = repositoriesItem.language,
            hasIssues = repositoriesItem.hasIssues,
            hasProjects = repositoriesItem.hasProjects,
            hasDownloads = repositoriesItem.hasDownloads,
            hasWiki = repositoriesItem.hasWiki,
            hasPages = repositoriesItem.hasPages,
            forksCount = repositoriesItem.forksCount,
            archived = repositoriesItem.archived,
            openIssuesCount = repositoriesItem.openIssuesCount,
            forks = repositoriesItem.forks,
            openIssues = repositoriesItem.openIssues,
            watchers = repositoriesItem.watchers,
            defaultBranch = repositoriesItem.defaultBranch,
            score = repositoriesItem.score
    )

    fun mapToPresentation(list: List<RepositoriesItem>): List<com.evgeny.presentation.model.RepositoriesItem> = list.map { mapToPresentation(it) }

    fun mapToDomain(repositoriesItem: com.evgeny.presentation.model.RepositoriesItem): RepositoriesItem = RepositoriesItem(
            id = repositoriesItem.id,
            nodeId = repositoriesItem.nodeId,
            name = repositoriesItem.name,
            owner = ownerItemMapper.mapToDomain(repositoriesItem.owner),
            fullName = repositoriesItem.fullName,
            _private = repositoriesItem._private,
            htmlUrl = repositoriesItem.htmlUrl,
            description = repositoriesItem.description,
            fork = repositoriesItem.fork,
            url = repositoriesItem.url,
            forksUrl = repositoriesItem.forksUrl,
            keysUrl = repositoriesItem.keysUrl,
            collaboratorsUrl = repositoriesItem.collaboratorsUrl,
            teamsUrl = repositoriesItem.teamsUrl,
            hooksUrl = repositoriesItem.hooksUrl,
            issueEventsUrl = repositoriesItem.issueEventsUrl,
            eventsUrl = repositoriesItem.eventsUrl,
            assigneesUrl = repositoriesItem.assigneesUrl,
            branchesUrl = repositoriesItem.branchesUrl,
            tagsUrl = repositoriesItem.tagsUrl,
            blobsUrl = repositoriesItem.blobsUrl,
            gitTagsUrl = repositoriesItem.gitTagsUrl,
            gitRefsUrl = repositoriesItem.gitRefsUrl,
            treesUrl = repositoriesItem.treesUrl,
            statusesUrl = repositoriesItem.statusesUrl,
            languagesUrl = repositoriesItem.languagesUrl,
            stargazersUrl = repositoriesItem.stargazersUrl,
            contributorsUrl = repositoriesItem.contributorsUrl,
            subscribersUrl = repositoriesItem.subscribersUrl,
            subscriptionUrl = repositoriesItem.subscriptionUrl,
            commitsUrl = repositoriesItem.commitsUrl,
            gitCommitsUrl = repositoriesItem.gitCommitsUrl,
            commentsUrl = repositoriesItem.commentsUrl,
            issueCommentUrl = repositoriesItem.issueCommentUrl,
            contentsUrl = repositoriesItem.contentsUrl,
            compareUrl = repositoriesItem.compareUrl,
            mergesUrl = repositoriesItem.mergesUrl,
            archiveUrl = repositoriesItem.archiveUrl,
            downloadsUrl = repositoriesItem.downloadsUrl,
            issuesUrl = repositoriesItem.issuesUrl,
            pullsUrl = repositoriesItem.pullsUrl,
            milestonesUrl = repositoriesItem.milestonesUrl,
            notificationsUrl = repositoriesItem.notificationsUrl,
            labelsUrl = repositoriesItem.labelsUrl,
            releasesUrl = repositoriesItem.releasesUrl,
            deploymentsUrl = repositoriesItem.deploymentsUrl,
            createdAt = repositoriesItem.createdAt,
            updatedAt = repositoriesItem.updatedAt,
            pushedAt = repositoriesItem.pushedAt,
            gitUrl = repositoriesItem.gitUrl,
            sshUrl = repositoriesItem.sshUrl,
            cloneUrl = repositoriesItem.cloneUrl,
            svnUrl = repositoriesItem.svnUrl,
            size = repositoriesItem.size,
            stargazersCount = repositoriesItem.stargazersCount,
            watchersCount = repositoriesItem.watchersCount,
            language = repositoriesItem.language,
            hasIssues = repositoriesItem.hasIssues,
            hasProjects = repositoriesItem.hasProjects,
            hasDownloads = repositoriesItem.hasDownloads,
            hasWiki = repositoriesItem.hasWiki,
            hasPages = repositoriesItem.hasPages,
            forksCount = repositoriesItem.forksCount,
            archived = repositoriesItem.archived,
            openIssuesCount = repositoriesItem.openIssuesCount,
            forks = repositoriesItem.forks,
            openIssues = repositoriesItem.openIssues,
            watchers = repositoriesItem.watchers,
            defaultBranch = repositoriesItem.defaultBranch,
            score = repositoriesItem.score
    )

    fun mapToDomain(list: List<com.evgeny.presentation.model.RepositoriesItem>): List<RepositoriesItem> = list.map { mapToDomain(it) }
}

data class OwnerItem(
        val login: String = "",
        val id: Int = 0,
        val avatarUrl: String = "",
        val gravatarId: String = "",
        val type: String = "",
        val siteAdmin: Boolean = false
) : Serializable


class OwnerItemMapper @Inject constructor(
) {

    fun mapToPresentation(entity: Owner): OwnerItem = OwnerItem(
            login = entity.login,
            id = entity.id,
            avatarUrl = entity.avatarUrl,
            gravatarId = entity.gravatarId,
            type = entity.type,
            siteAdmin = entity.siteAdmin
    )

    fun mapToPresentation(list: List<Owner>): List<OwnerItem> = list.map { mapToPresentation(it) }

    fun mapToDomain(entity: OwnerItem): Owner = Owner(
            login = entity.login,
            id = entity.id,
            avatarUrl = entity.avatarUrl,
            gravatarId = entity.gravatarId,
            type = entity.type,
            siteAdmin = entity.siteAdmin
    )

    fun mapToDomain(list: List<OwnerItem>): List<Owner> = list.map { mapToDomain(it) }
}

