package com.evgeny.presentation.di

import com.evgeny.presentation.reposytory.RepositoryActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, ViewModelModule::class, NetworkModule::class, RepositoryModule::class])
interface Injector {
    fun inject(activity: RepositoryActivity)



}
