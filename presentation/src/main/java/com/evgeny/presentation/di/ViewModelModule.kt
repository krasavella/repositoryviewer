package com.evgeny.presentation.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.evgeny.presentation.ViewModelFactory
import com.evgeny.presentation.ViewModelKey
import com.evgeny.presentation.reposytory.RepositoryViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(RepositoryViewModel::class)
    internal abstract fun repositoryViewModel(viewModel: RepositoryViewModel): ViewModel
}
