package com.evgeny.presentation.di

import com.evgeny.data.repository.*
import com.evgeny.domain.repository.*
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {
    @Binds
    abstract fun bindRepository(repository: RepositoryImpl): GitRepository
}
