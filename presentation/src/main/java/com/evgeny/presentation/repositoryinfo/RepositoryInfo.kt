package com.evgeny.presentation.repositoryinfo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.evgeny.presentation.*
import com.evgeny.presentation.model.REPOSITORIES_ID_KEY
import com.evgeny.presentation.model.RepositoriesItem
import kotlinx.android.synthetic.main.activity_repository_info.*

class RepositoryInfo : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repository_info)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val b = intent.extras
        val repository = b!!.getSerializable(REPOSITORIES_ID_KEY) as RepositoriesItem
        title = getString(R.string.title_repository_info)
        countStars.text =  getString(R.string.count_star, repository.stargazersCount.toString())
        countFork.text = getString(R.string.count_fork, repository.forksCount.toString())
        repositoryName.text = getString(R.string.name_repository, repository.name.toString())
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}