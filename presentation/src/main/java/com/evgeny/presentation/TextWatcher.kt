package com.evgeny.presentation

import android.text.Editable
import android.text.TextWatcher

open class TextWatcher : TextWatcher {
    override fun afterTextChanged(editable: Editable?) {
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }
}