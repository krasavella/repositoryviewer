package com.evgeny.presentation.navigation

import android.app.Activity
import android.content.Intent
import com.evgeny.presentation.repositoryinfo.RepositoryInfo
import javax.inject.Inject
import android.os.Bundle
import com.evgeny.presentation.model.REPOSITORIES_ID_KEY
import com.evgeny.presentation.model.RepositoriesItem


class RepositoryNavigator @Inject constructor() {
    fun navigate(activity: Activity, repositoriesItem: RepositoriesItem) {
        val intent = Intent(activity, RepositoryInfo::class.java)
        val bundle = Bundle()
        bundle.putSerializable(REPOSITORIES_ID_KEY, repositoriesItem)
        intent.putExtras(bundle)
        activity.startActivity(intent)
    }
}

