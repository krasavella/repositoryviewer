package com.evgeny.presentation.reposytory

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.evgeny.presentation.R
import com.evgeny.presentation.inflate
import com.evgeny.presentation.loadAvatar
import com.evgeny.presentation.model.RepositoriesItem
import kotlinx.android.synthetic.main.item_list_repository.view.*
import java.util.ArrayList
import javax.inject.Inject
import com.evgeny.presentation.navigation.RepositoryNavigator


class RepositoryAdapter @Inject constructor(private val itemClick: (RepositoriesItem) -> Unit) : RecyclerView.Adapter<RepositoryAdapter.ViewHolder>() {

    private val items = ArrayList<RepositoriesItem>()
    private var repositoryNavigator = RepositoryNavigator()
    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    inner class ViewHolder(parent: ViewGroup) :
            RecyclerView.ViewHolder(parent.inflate(R.layout.item_list_repository)) {

        fun bind(item: RepositoriesItem) {
            itemView.ownerName.text = item.owner.login
            itemView.repositoryName.text = item.name
            itemView.language.text = item.language
            itemView.avatar.loadAvatar(item.owner.avatarUrl)
            itemView.list_item.setOnClickListener {
                itemClick.invoke(item)
            }
        }
    }

    fun setNavigator(repositoryNavigator: RepositoryNavigator) {
        this.repositoryNavigator = repositoryNavigator
    }

    fun addItems(list: List<RepositoriesItem>?, totalItemCount: Int) {
        if (list == null) {
            this.items.clear()
            notifyDataSetChanged()
        } else {
            if (totalItemCount == 1) {
                this.items.clear()
                this.items.addAll(list!!)
            } else {
                this.items.addAll(list!!)
            }
            if (totalItemCount == 1) {
                notifyDataSetChanged()
            } else {
                notifyItemInserted(totalItemCount)
            }
        }

    }
}