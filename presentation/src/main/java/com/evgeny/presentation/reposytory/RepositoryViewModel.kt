package com.evgeny.presentation.reposytory

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.evgeny.domain.usecase.RepositoryUseCase
import com.evgeny.presentation.Data
import com.evgeny.presentation.DataState
import com.evgeny.presentation.model.RepositoryItem
import com.evgeny.presentation.model.RepositoryItemMapper
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RepositoryViewModel @Inject constructor(
private val useCase: RepositoryUseCase,
private val mapper: RepositoryItemMapper
) : ViewModel() {

    val posts = MutableLiveData<Data<RepositoryItem>>()
    val compositeDisposable = CompositeDisposable()

    fun get(repositoryName: String, refresh: Boolean = false, page : Int) =
            compositeDisposable.add(useCase.getRepository(repositoryName, refresh, page)
                    .doOnSubscribe {
                        posts.postValue(
                                Data(
                                        dataState = DataState.LOADING,
                                        data = posts.value?.data,
                                        message = null
                                )
                        )
                    }
                    .subscribeOn(Schedulers.io())
                    .map { mapper.mapToPresentation(it) }
                    .subscribe(
                            {
                                posts.postValue(Data(dataState = DataState.SUCCESS, data = it, message = null))
                            },
                            {
                                posts.postValue(
                                        Data(
                                                dataState = DataState.ERROR,
                                                data = posts.value?.data,
                                                message = it.message
                                        )
                                )
                            })
            )


    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }

}
