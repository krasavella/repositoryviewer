package com.evgeny.presentation.reposytory

import android.arch.lifecycle.ViewModelProvider
import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.text.Editable
import com.evgeny.presentation.*
import com.evgeny.presentation.adapter.base.EndlessRecyclerOnScrollListener
import com.evgeny.presentation.model.RepositoriesItem
import com.evgeny.presentation.model.RepositoryItem
import com.evgeny.presentation.navigation.RepositoryNavigator
import kotlinx.android.synthetic.main.activity_repository.*
import org.jetbrains.anko.toast
import java.util.*
import javax.inject.Inject

class RepositoryActivity : AppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var repositoryNavigator: RepositoryNavigator
    private val itemClick: (RepositoriesItem) -> Unit = {
        repositoryNavigator.navigate(this, it)
    }
    private var oldSearch = ""
    private val adapter = RepositoryAdapter(itemClick)
    private var loadedItems = 1
    private var oldId = 0
    private var oldTime = 0L
    private val handler = Handler(Looper.getMainLooper())
    private val handlerError = Handler(Looper.getMainLooper())
    private var workRunnable: Runnable? = null
    private var workRunnableError: Runnable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repository)
        getAppInjector().inject(this)
        if (savedInstanceState != null) {
            loadedItems = savedInstanceState.getInt(LOADED_ITEM)
            oldSearch = savedInstanceState.getString(OLD_SEARCH)
        }
        repositoriesRW.adapter = adapter
        initRecyclerView()
        adapter.setNavigator(repositoryNavigator)
        swipeRefreshRepository.setOnRefreshListener {
            swipeRefreshRepository.stopRefreshing()
        }
        withViewModel<RepositoryViewModel>(viewModelFactory)
        {
            searchRepository.addTextChangedListener(object : com.evgeny.presentation.TextWatcher() {
                override fun afterTextChanged(editable: Editable?) {
                    loadedItems = 1
                    if (editable.toString() != "") {
                        loadData(editable.toString(), loadedItems)
                    } else {
                        adapter.addItems(null, 0)
                    }
                }
            })
            observe(posts, ::updateRepository)
        }
    }

    private fun initRecyclerView() {
        val gridLayoutManager = GridLayoutManager(applicationContext, 1)
        repositoriesRW.layoutManager = gridLayoutManager
        val scrollListener = object : EndlessRecyclerOnScrollListener() {
            override fun onLoadMore() {
                withViewModel<RepositoryViewModel>(viewModelFactory) {
                    if (searchRepository.text.toString() != "") {
                        loadData(searchRepository.text.toString(), loadedItems)
                    }
                    observe(posts, ::updateRepository)
                }
            }
        }
        repositoriesRW.addOnScrollListener(scrollListener)
    }

    private fun loadData(searchText: String, page: Int) {
        withViewModel<RepositoryViewModel>(viewModelFactory)
        {
            handler.removeCallbacks(workRunnable)
            workRunnable = Runnable { get(searchText, refresh = true, page = page) }
            compositeDisposable.clear()
            handler.postDelayed(workRunnable, DELAY)
            observe(posts, ::updateRepository)
        }
    }

    private fun updateRepository(data: Data<RepositoryItem>?) {
        data?.let {
            when (it.dataState) {
                DataState.LOADING -> {
                    swipeRefreshRepository.startRefreshing()
                }
                DataState.SUCCESS -> {
                    swipeRefreshRepository.stopRefreshing()
                }
                DataState.ERROR -> {
                    swipeRefreshRepository.stopRefreshing()
                }
            }
            it.data?.let {
                if (!it.items.isEmpty() && it.items[0].id != (oldId)) {
                    adapter.addItems(it.items, loadedItems)
                    oldId = it.items[0].id!!
                    loadedItems++
                    return
                }
                if (it.items.isEmpty()) {
                    adapter.addItems(null, 0)
                }

            }
            it.message?.let {
                if (oldTime != it.toLong()) {
                    val timeReset = it.toLong()
                    oldTime = timeReset
                    workRunnableError = Runnable {
                        searchRepository.isEnabled = true
                        searchRepository.error = null
                        loadData(searchText = searchRepository.text.toString(), page = loadedItems)
                    }
                    val newDate = Date().time
                    val millis = ((timeReset * MINUTE) - newDate)
                    val isBlock = newDate.compareTo(it.toLong() * MINUTE)
                    when (isBlock) {
                        -1 -> {
                            val toastTime = millis / MINUTE
                            searchRepository.isEnabled = false
                            searchRepository.error = getString(R.string.restriction)
                            handlerError.postDelayed(workRunnableError, millis)
                            toast(getString(R.string.wait, toastTime.toString()))
                            return
                        }
                    }
                }
            }
        }
    }

    override fun onSaveInstanceState(icicle: Bundle) {
        super.onSaveInstanceState(icicle)
        icicle.putInt(LOADED_ITEM, loadedItems)
        icicle.putString(OLD_SEARCH, searchRepository.text.toString())
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        adapter.notifyDataSetChanged()
    }

    companion object {
        const val DELAY = 500L
        const val MINUTE = 1000L
        const val LOADED_ITEM = "loadedItems"
        const val OLD_SEARCH = "oldSearch"
    }

}
