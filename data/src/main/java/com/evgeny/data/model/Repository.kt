package com.evgeny.data.model

import com.evgeny.domain.model.RepositoriesItem
import com.evgeny.domain.model.Owner
import com.evgeny.domain.model.Repository
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.inject.Inject

data class RepositoryEntity(@SerializedName("total_count")
                            val totalCount: Int = 0,
                            @SerializedName("incomplete_results")
                            val incompleteResults: Boolean = false,
                            @SerializedName("items")
                            val items: List<RepositoriesEntity>) : Serializable


class RepositoryMapper @Inject constructor(
        private val repositoriesItemMapper: RepositoriesItemMapper
) {
    fun mapToDomain(entity: RepositoryEntity): Repository = Repository(
            totalCount = entity.totalCount,
            incompleteResults = entity.incompleteResults,
            items = repositoriesItemMapper.mapToDomain(entity.items)
    )

    fun mapToDomain(list: List<RepositoryEntity>): List<Repository> = list.map { mapToDomain(it) }

    fun mapToEntity(repository: Repository): RepositoryEntity = RepositoryEntity(
            totalCount = repository.totalCount,
            incompleteResults = repository.incompleteResults,
            items = repositoriesItemMapper.mapToEntity(repository.items)
    )

    fun mapToEntity(list: List<Repository>): List<RepositoryEntity> = list.map { mapToEntity(it) }
}


data class RepositoriesEntity(@SerializedName("id")
                              val id: Int? = 0,
                              @SerializedName("owner")
                              val owner: OwnerEntity,
                              @SerializedName("node_id")
                              val nodeId: String? = "",
                              @SerializedName("name")
                              val name: String? = "",
                              @SerializedName("full_name")
                              val fullName: String?,
                              @SerializedName("private")
                              val _private: Boolean? = false,
                              @SerializedName("html_url")
                              val htmlUrl: String? = "",
                              @SerializedName("description")
                              val description: String? = "",
                              @SerializedName("fork")
                              val fork: Boolean? = false,
                              @SerializedName("url")
                              val url: String? = "",
                              @SerializedName("forks_url")
                              val forksUrl: String? = "",
                              @SerializedName("keys_url")
                              val keysUrl: String? = "",
                              @SerializedName("collaborators_url")
                              val collaboratorsUrl: String? = "",
                              @SerializedName("teams_url")
                              val teamsUrl: String? = "",
                              @SerializedName("hooks_url")
                              val hooksUrl: String? = "",
                              @SerializedName("issue_events_url")
                              val issueEventsUrl: String? = "",
                              @SerializedName("events_url")
                              val eventsUrl: String? = "",
                              @SerializedName("assignees_url")
                              val assigneesUrl: String? = "",
                              @SerializedName("branches_url")
                              val branchesUrl: String? = "",
                              @SerializedName("tags_url")
                              val tagsUrl: String? = "",
                              @SerializedName("blobs_url")
                              val blobsUrl: String? = "",
                              @SerializedName("git_tags_url")
                              val gitTagsUrl: String? = "",
                              @SerializedName("git_refs_url")
                              val gitRefsUrl: String? = "",
                              @SerializedName("trees_url")
                              val treesUrl: String? = "",
                              @SerializedName("statuses_url")
                              val statusesUrl: String? = "",
                              @SerializedName("languages_url")
                              val languagesUrl: String? = "",
                              @SerializedName("stargazers_url")
                              val stargazersUrl: String? = "",
                              @SerializedName("contributors_url")
                              val contributorsUrl: String? = "",
                              @SerializedName("subscribers_url")
                              val subscribersUrl: String? = "",
                              @SerializedName("subscription_url")
                              val subscriptionUrl: String? = "",
                              @SerializedName("commits_url")
                              val commitsUrl: String? = "",
                              @SerializedName("git_commits_url")
                              val gitCommitsUrl: String? = "",
                              @SerializedName("comments_url")
                              val commentsUrl: String? = "",
                              @SerializedName("issue_comment_url")
                              val issueCommentUrl: String? = "",
                              @SerializedName("contents_url")
                              val contentsUrl: String? = "",
                              @SerializedName("compare_url")
                              val compareUrl: String? = "",
                              @SerializedName("merges_url")
                              val mergesUrl: String? = "",
                              @SerializedName("archive_url")
                              val archiveUrl: String? = "",
                              @SerializedName("downloads_url")
                              val downloadsUrl: String? = "",
                              @SerializedName("issues_url")
                              val issuesUrl: String? = "",
                              @SerializedName("pulls_url")
                              val pullsUrl: String? = "",
                              @SerializedName("milestones_url")
                              val milestonesUrl: String? = "",
                              @SerializedName("notifications_url")
                              val notificationsUrl: String? = "",
                              @SerializedName("labels_url")
                              val labelsUrl: String? = "",
                              @SerializedName("releases_url")
                              val releasesUrl: String? = "",
                              @SerializedName("deployments_url")
                              val deploymentsUrl: String? = "",
                              @SerializedName("created_at")
                              val createdAt: String? = "",
                              @SerializedName("updated_at")
                              val updatedAt: String? = "",
                              @SerializedName("pushed_at")
                              val pushedAt: String? = "",
                              @SerializedName("git_url")
                              val gitUrl: String? = "",
                              @SerializedName("ssh_url")
                              val sshUrl: String? = "",
                              @SerializedName("clone_url")
                              val cloneUrl: String? = "",
                              @SerializedName("svn_url")
                              val svnUrl: String? = "",
                              @SerializedName("size")
                              val size: Int? = 0,
                              @SerializedName("stargazers_count")
                              val stargazersCount: Int? = 0,
                              @SerializedName("watchers_count")
                              val watchersCount: Int? = 0,
                              @SerializedName("language")
                              val language: String? = "",
                              @SerializedName("has_issues")
                              val hasIssues: Boolean? = false,
                              @SerializedName("has_projects")
                              val hasProjects: Boolean? = false,
                              @SerializedName("has_downloads")
                              val hasDownloads: Boolean? = false,
                              @SerializedName("has_wiki")
                              val hasWiki: Boolean? = false,
                              @SerializedName("has_pages")
                              val hasPages: Boolean? = false,
                              @SerializedName("forks_count")
                              val forksCount: Int? = 0,
                              @SerializedName("archived")
                              val archived: Boolean? = false,
                              @SerializedName("open_issues_count")
                              val openIssuesCount: Int? = 0,
                              @SerializedName("forks")
                              val forks: Int? = 0,
                              @SerializedName("open_issues")
                              val openIssues: Int? = 0,
                              @SerializedName("watchers")
                              val watchers: Int? = 0,
                              @SerializedName("default_branch")
                              val defaultBranch: String? = "",
                              @SerializedName("score")
                              val score: Double? = 0.0) : Serializable

class RepositoriesItemMapper @Inject constructor(
        private val ownerMapper: OwnerMapper
) {
    fun mapToDomain(repositories: RepositoriesEntity): RepositoriesItem = RepositoriesItem(
            id = repositories.id,
            nodeId = repositories.nodeId,
            name = repositories.name,
            owner = ownerMapper.mapToDomain(repositories.owner),
            fullName = repositories.fullName,
            _private = repositories._private,
            htmlUrl = repositories.htmlUrl,
            description = repositories.description,
            fork = repositories.fork,
            url = repositories.url,
            forksUrl = repositories.forksUrl,
            keysUrl = repositories.keysUrl,
            collaboratorsUrl = repositories.collaboratorsUrl,
            teamsUrl = repositories.teamsUrl,
            hooksUrl = repositories.hooksUrl,
            issueEventsUrl = repositories.issueEventsUrl,
            eventsUrl = repositories.eventsUrl,
            assigneesUrl = repositories.assigneesUrl,
            branchesUrl = repositories.branchesUrl,
            tagsUrl = repositories.tagsUrl,
            blobsUrl = repositories.blobsUrl,
            gitTagsUrl = repositories.gitTagsUrl,
            gitRefsUrl = repositories.gitRefsUrl,
            treesUrl = repositories.treesUrl,
            statusesUrl = repositories.statusesUrl,
            languagesUrl = repositories.languagesUrl,
            stargazersUrl = repositories.stargazersUrl,
            contributorsUrl = repositories.contributorsUrl,
            subscribersUrl = repositories.subscribersUrl,
            subscriptionUrl = repositories.subscriptionUrl,
            commitsUrl = repositories.commitsUrl,
            gitCommitsUrl = repositories.gitCommitsUrl,
            commentsUrl = repositories.commentsUrl,
            issueCommentUrl = repositories.issueCommentUrl,
            contentsUrl = repositories.contentsUrl,
            compareUrl = repositories.compareUrl,
            mergesUrl = repositories.mergesUrl,
            archiveUrl = repositories.archiveUrl,
            downloadsUrl = repositories.downloadsUrl,
            issuesUrl = repositories.issuesUrl,
            pullsUrl = repositories.pullsUrl,
            milestonesUrl = repositories.milestonesUrl,
            notificationsUrl = repositories.notificationsUrl,
            labelsUrl = repositories.labelsUrl,
            releasesUrl = repositories.releasesUrl,
            deploymentsUrl = repositories.deploymentsUrl,
            createdAt = repositories.createdAt,
            updatedAt = repositories.updatedAt,
            pushedAt = repositories.pushedAt,
            gitUrl = repositories.gitUrl,
            sshUrl = repositories.sshUrl,
            cloneUrl = repositories.cloneUrl,
            svnUrl = repositories.svnUrl,
            size = repositories.size,
            stargazersCount = repositories.stargazersCount,
            watchersCount = repositories.watchersCount,
            language = repositories.language,
            hasIssues = repositories.hasIssues,
            hasProjects = repositories.hasProjects,
            hasDownloads = repositories.hasDownloads,
            hasWiki = repositories.hasWiki,
            hasPages = repositories.hasPages,
            forksCount = repositories.forksCount,
            archived = repositories.archived,
            openIssuesCount = repositories.openIssuesCount,
            forks = repositories.forks,
            openIssues = repositories.openIssues,
            watchers = repositories.watchers,
            defaultBranch = repositories.defaultBranch,
            score = repositories.score
    )

    fun mapToDomain(list: List<RepositoriesEntity>): List<RepositoriesItem> = list.map { mapToDomain(it) }

    fun mapToEntity(repositoriesItem: RepositoriesItem): RepositoriesEntity = RepositoriesEntity(
            id = repositoriesItem.id,
            nodeId = repositoriesItem.nodeId,
            name = repositoriesItem.name,
            owner = ownerMapper.mapToEntity(repositoriesItem.owner),
            fullName = repositoriesItem.fullName,
            _private = repositoriesItem._private,
            htmlUrl = repositoriesItem.htmlUrl,
            description = repositoriesItem.description,
            fork = repositoriesItem.fork,
            url = repositoriesItem.url,
            forksUrl = repositoriesItem.forksUrl,
            keysUrl = repositoriesItem.keysUrl,
            collaboratorsUrl = repositoriesItem.collaboratorsUrl,
            teamsUrl = repositoriesItem.teamsUrl,
            hooksUrl = repositoriesItem.hooksUrl,
            issueEventsUrl = repositoriesItem.issueEventsUrl,
            eventsUrl = repositoriesItem.eventsUrl,
            assigneesUrl = repositoriesItem.assigneesUrl,
            branchesUrl = repositoriesItem.branchesUrl,
            tagsUrl = repositoriesItem.tagsUrl,
            blobsUrl = repositoriesItem.blobsUrl,
            gitTagsUrl = repositoriesItem.gitTagsUrl,
            gitRefsUrl = repositoriesItem.gitRefsUrl,
            treesUrl = repositoriesItem.treesUrl,
            statusesUrl = repositoriesItem.statusesUrl,
            languagesUrl = repositoriesItem.languagesUrl,
            stargazersUrl = repositoriesItem.stargazersUrl,
            contributorsUrl = repositoriesItem.contributorsUrl,
            subscribersUrl = repositoriesItem.subscribersUrl,
            subscriptionUrl = repositoriesItem.subscriptionUrl,
            commitsUrl = repositoriesItem.commitsUrl,
            gitCommitsUrl = repositoriesItem.gitCommitsUrl,
            commentsUrl = repositoriesItem.commentsUrl,
            issueCommentUrl = repositoriesItem.issueCommentUrl,
            contentsUrl = repositoriesItem.contentsUrl,
            compareUrl = repositoriesItem.compareUrl,
            mergesUrl = repositoriesItem.mergesUrl,
            archiveUrl = repositoriesItem.archiveUrl,
            downloadsUrl = repositoriesItem.downloadsUrl,
            issuesUrl = repositoriesItem.issuesUrl,
            pullsUrl = repositoriesItem.pullsUrl,
            milestonesUrl = repositoriesItem.milestonesUrl,
            notificationsUrl = repositoriesItem.notificationsUrl,
            labelsUrl = repositoriesItem.labelsUrl,
            releasesUrl = repositoriesItem.releasesUrl,
            deploymentsUrl = repositoriesItem.deploymentsUrl,
            createdAt = repositoriesItem.createdAt,
            updatedAt = repositoriesItem.updatedAt,
            pushedAt = repositoriesItem.pushedAt,
            gitUrl = repositoriesItem.gitUrl,
            sshUrl = repositoriesItem.sshUrl,
            cloneUrl = repositoriesItem.cloneUrl,
            svnUrl = repositoriesItem.svnUrl,
            size = repositoriesItem.size,
            stargazersCount = repositoriesItem.stargazersCount,
            watchersCount = repositoriesItem.watchersCount,
            language = repositoriesItem.language,
            hasIssues = repositoriesItem.hasIssues,
            hasProjects = repositoriesItem.hasProjects,
            hasDownloads = repositoriesItem.hasDownloads,
            hasWiki = repositoriesItem.hasWiki,
            hasPages = repositoriesItem.hasPages,
            forksCount = repositoriesItem.forksCount,
            archived = repositoriesItem.archived,
            openIssuesCount = repositoriesItem.openIssuesCount,
            forks = repositoriesItem.forks,
            openIssues = repositoriesItem.openIssues,
            watchers = repositoriesItem.watchers,
            defaultBranch = repositoriesItem.defaultBranch,
            score = repositoriesItem.score
    )

    fun mapToEntity(itemItems: List<RepositoriesItem>): List<RepositoriesEntity> = itemItems.map { mapToEntity(it) }
}

data class OwnerEntity(@SerializedName("login")
                       val login: String = "",
                       @SerializedName("id")
                       @Expose
                       val id: Int = 0,
                       @SerializedName("avatar_url")
                       @Expose
                       val avatarUrl: String = "",
                       @SerializedName("gravatar_id")
                       val gravatarId: String = "",
                       @SerializedName("type")
                       val type: String = "",
                       @SerializedName("siteAdmin")
                       val siteAdmin: Boolean = false) : Serializable

class OwnerMapper @Inject constructor(
) {
    fun mapToDomain(entity: OwnerEntity): Owner = Owner(
            login = entity.login,
            id = entity.id,
            avatarUrl = entity.avatarUrl,
            gravatarId = entity.gravatarId,
            type = entity.type,
            siteAdmin = entity.siteAdmin)


    fun mapToDomain(list: List<OwnerEntity>): List<Owner> = list.map { mapToDomain(it) }

    fun mapToEntity(entity: Owner): OwnerEntity = OwnerEntity(
            login = entity.login,
            id = entity.id,
            avatarUrl = entity.avatarUrl,
            gravatarId = entity.gravatarId,
            type = entity.type,
            siteAdmin = entity.siteAdmin
    )

    fun mapToEntity(list: List<Owner>): List<OwnerEntity> = list.map { mapToEntity(it) }
}