package com.evgeny.data.model

import com.evgeny.domain.model.Limit
import com.evgeny.domain.model.Resources
import com.evgeny.domain.model.Search
import com.google.gson.annotations.SerializedName
import javax.inject.Inject

data class LimitEntity(@SerializedName("resources")
                       val resources: ResourcesEntity)

class LimitEntityMapper @Inject constructor(
        private val resourcesEntityMapper: ResourcesEntityMapper
) {
    fun mapToDomain(entity: LimitEntity): Limit = Limit(
            resources = resourcesEntityMapper.mapToDomain(entity.resources)
    )

    fun mapToDomain(list: List<LimitEntity>): List<Limit> = list.map { mapToDomain(it) }

    fun mapToEntity(entity: Limit): LimitEntity = LimitEntity(
            resources = resourcesEntityMapper.mapToEntity(entity.resources)
    )

    fun mapToEntity(list: List<Limit>): List<LimitEntity> = list.map { mapToEntity(it) }
}


data class ResourcesEntity(@SerializedName("search")
                           val search: SearchEntity)

class ResourcesEntityMapper @Inject constructor(
        private val searchEntityMapper: SearchEntityMapper
) {
    fun mapToDomain(entity: ResourcesEntity): Resources = Resources(
            search = searchEntityMapper.mapToDomain(entity.search)
    )

    fun mapToDomain(list: List<ResourcesEntity>): List<Resources> = list.map { mapToDomain(it) }

    fun mapToEntity(entity: Resources): ResourcesEntity = ResourcesEntity(
            search = searchEntityMapper.mapToEntity(entity.search)
    )

    fun mapToEntity(list: List<Resources>): List<ResourcesEntity> = list.map { mapToEntity(it) }
}


data class SearchEntity(@SerializedName("limit")
                        val limit: Int = 0,
                        @SerializedName("remaining")
                        val remaining: Int = 0,
                        @SerializedName("reset")
                        val reset: Int = 0)

class SearchEntityMapper @Inject constructor(
) {
    fun mapToDomain(entity: SearchEntity): Search = Search(
            limit = entity.limit,
            remaining = entity.remaining,
            reset = entity.reset
    )

    fun mapToDomain(list: List<SearchEntity>): List<Search> = list.map { mapToDomain(it) }

    fun mapToEntity(entity: Search): SearchEntity = SearchEntity(
            limit = entity.limit,
            remaining = entity.remaining,
            reset = entity.reset
    )

    fun mapToEntity(list: List<Search>): List<SearchEntity> = list.map { mapToEntity(it) }
}