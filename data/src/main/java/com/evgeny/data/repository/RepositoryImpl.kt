package com.evgeny.data.repository

import com.evgeny.data.cache.Cache
import com.evgeny.data.model.LimitEntityMapper
import com.evgeny.data.model.RepositoryEntity
import com.evgeny.data.model.RepositoryMapper
import com.evgeny.data.remote.GitApi
import com.evgeny.domain.model.Limit
import com.evgeny.domain.model.Repository
import com.evgeny.domain.repository.GitRepository
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RepositoryImpl @Inject constructor(
        private val api: GitApi,
        private val cache: Cache<List<RepositoryEntity>>,
        private val repositoryMapper: RepositoryMapper,
        private val limitEntityMapper: LimitEntityMapper

) : GitRepository {
    override val key = "Repository"

    override fun get(repositoryName: String, refresh: Boolean, page: Int): Single<Repository> =
            when (refresh) {
                true ->
                    api.getRepositories(repositoryName, page)
                            .map { repositoryMapper.mapToDomain(it) }
                false -> TODO()
            }

    fun get(refresh: Boolean): Single<List<Repository>> =
            when (refresh) {
                false -> cache.load(key)
                        .map { repositoryMapper.mapToDomain(it) }
                        .onErrorResumeNext { get(true) }
                true -> TODO()
            }

    override fun getLimit(): Single<Limit> =
            api.getLimit()
                    .map { limitEntityMapper.mapToDomain(it) }
}