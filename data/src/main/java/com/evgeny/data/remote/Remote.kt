package com.evgeny.data.remote

import com.evgeny.data.model.*
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GitApi {
    @GET("search/repositories")
    fun getRepositories(@Query("q") username: String,
                        @Query("page") page: Int): Single<RepositoryEntity>

    @GET("rate_limit")
    fun getLimit(): Single<LimitEntity>

}
