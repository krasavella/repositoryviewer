package com.evgeny.domain.usecase

import com.evgeny.domain.model.Repository
import com.evgeny.domain.repository.GitRepository
import io.reactivex.Single
import javax.inject.Inject

class RepositoryUseCase @Inject constructor(private val repository: GitRepository) {
    fun getRepository(repositoryName: String, refresh: Boolean, page: Int): Single<Repository> {
        return repository.getLimit().flatMap { limit ->
            if (limit.resources.search.remaining >= 1) {
                return@flatMap repository.get(repositoryName, refresh, page)
            } else {
                throw IllegalArgumentException(limit.resources.search.reset.toString())
            }
        }
    }
}
