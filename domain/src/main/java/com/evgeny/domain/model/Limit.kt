package com.evgeny.domain.model

data class Limit(val resources: Resources)

data class Resources(val search: Search)

data class Search(val limit: Int = 0,
                  val remaining: Int = 0,
                  val reset: Int = 0)