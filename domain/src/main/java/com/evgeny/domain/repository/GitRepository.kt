package com.evgeny.domain.repository

import com.evgeny.domain.model.Limit
import com.evgeny.domain.model.Repository
import io.reactivex.Single

interface GitRepository {
    val key: String
    fun get(repositoryName: String, refresh: Boolean, page: Int): Single<Repository>
    fun getLimit(): Single<Limit>
}
